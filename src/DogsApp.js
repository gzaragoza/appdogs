import React, { useState } from "react";
import { DogsImagesGallery } from "./components/dogsImagesGallery/DogsImagesGallery";
import { SelectBreed } from "./components/selectBreed/SelectBreed";
import { Header } from "./components/header/Header";

export const DogsApp = () => {
  const [breed, setBreed] = useState([]);

  return (
    <div>
      <Header />
      <SelectBreed setBreed={setBreed} />
      <hr />
      <DogsImagesGallery breed={breed} />
    </div>
  );
};
