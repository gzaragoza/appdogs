export const getDogsImages = async (breed) => {
  const url = `https://dog.ceo/api/breed/${breed}/images`;
  const response = await fetch(url);

  if (response.status >= 200 && response.status <= 299) {
    const data = await response.json();
    const breeds = data.message;
    return breeds;
  } else {
    return ["error"];
  }
};
