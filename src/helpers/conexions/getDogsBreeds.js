export const getDogsBreeds = async () => {
  const url = "https://dog.ceo/api/breeds/list/all";
  const response = await fetch(url);
  const data = await response.json();
  let breeds = [];

  if (response.status >= 200 && response.status <= 299) {
    Object.keys(data.message).map((breed) => (breeds = [...breeds, breed]));
    return breeds;
  } else {
    return ["error"];
  }
};
