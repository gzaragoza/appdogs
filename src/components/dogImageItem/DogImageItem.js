import React from "react";

export const DogImageItem = ({ img }) => {
  return (
    <>
      <img src={img} alt={img} width="100" />
    </>
  );
};
