import { DogImageItem } from "../dogImageItem/DogImageItem";
import { useFetchDogsImages } from "../hooks/useFetchDogsImages";
import { useTranslation } from "react-i18next";

export const DogsImagesGallery = ({ breed }) => {
  const { t } = useTranslation("common");
  const { data: imgs, loading } = useFetchDogsImages(breed);

  return (
    <>
      <div>
        {breed.length > 0 && (
          <h4>
            {" "}
            {t("dogsImagesGallery.resultsFor")}: {breed}
          </h4>
        )}
      </div>

      <div>
        {imgs[0] !== "error"
          ? imgs.map((img) => <DogImageItem key={img} img={img} />)
          : "Error fetching data"}
      </div>
      <div>{loading && <h3>{t("dogsImagesGallery.loading")}</h3>}</div>
    </>
  );
};
