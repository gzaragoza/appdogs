import React, { useEffect, useState } from "react";
import { getDogsBreeds } from "../../helpers/conexions/getDogsBreeds";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";

export const SelectBreed = ({ setBreed }) => {
  const [dogsBreeds, setDogsBreeds] = useState([]);

  useEffect(() => {
    getDogsBreeds().then((breeds) => setDogsBreeds(breeds));
  }, []);

  const handleSelectChange = (e) => {
    if (e.target.value !== "") setBreed(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  const { t } = useTranslation("common");
  return (
    <div>
      {dogsBreeds[0] !== "error" ? (
        <form onSubmit={handleSubmit}>
          <label> {t("selectBreed.selector")}</label>
          <select id="breeds" name="breedslist" onChange={handleSelectChange}>
            <option value=""></option>
            {dogsBreeds.map((breeds) => (
              <option key={breeds} value={breeds}>
                {breeds}
              </option>
            ))}
          </select>
        </form>
      ) : (
        "Error fetching data"
      )}
    </div>
  );
};

SelectBreed.propTypes = {
  setBreed: PropTypes.func.isRequired,
};
