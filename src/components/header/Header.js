import React, { useState } from "react";
import { useTranslation } from "react-i18next";

export const Header = () => {
  const [activeButton, setActiveButton] = useState("en");
  const { t, i18n } = useTranslation("common");

  return (
    <>
      <div className="translationHeaderButtons">
        <button
          className={
            activeButton === "es" ? "translationButtonSelected" : undefined
          }
          onClick={() => {
            i18n.changeLanguage("es");
            setActiveButton("es");
          }}
        >
          es
        </button>
        <button
          className={
            activeButton === "en" ? "translationButtonSelected" : undefined
          }
          onClick={() => {
            i18n.changeLanguage("en");
            setActiveButton("en");
          }}
        >
          en
        </button>
      </div>
      <div>
        <h3>{t("header.title")}</h3>
      </div>
    </>
  );
};
