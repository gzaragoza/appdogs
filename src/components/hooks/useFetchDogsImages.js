import { useState, useEffect } from "react";
import { getDogsImages } from "../../helpers/conexions/getDogsImages";

export const useFetchDogsImages = (breed) => {
  const [state, setState] = useState({
    data: [],
    loading: false,
  });

  useEffect(() => {
    if (breed.length > 0) {
      setState({
        data: [],
        loading: true,
      });

      getDogsImages(breed).then((imgs) => {
        setState({
          data: imgs,
          loading: false,
        });
      });
    }
  }, [breed]);

  return state;
};
